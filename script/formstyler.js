/*
 * jQuery Form Styler v1.5.1
 * https://github.com/Dimox/jQueryFormStyler
 *
 * Copyright 2012-2014 Dimox (http://dimox.name/)
 * Released under the MIT license.
 *
 * Date: 2014.05.03
 *
 */

(function($) {
	$.fn.styler = function(opt) {

		var opt = $.extend({
			wrapper: 'form',
			idSuffix: '-styler',
			filePlaceholder: 'Ð¤Ð°Ð¹Ð» Ð½Ðµ Ð²Ñ‹Ð±Ñ€Ð°Ð½',
			fileBrowse: 'ÐžÐ±Ð·Ð¾Ñ€...',
			selectSearch: true,
			selectSearchLimit: 10,
			selectSearchNotFound: 'Ð¡Ð¾Ð²Ð¿Ð°Ð´ÐµÐ½Ð¸Ð¹ Ð½Ðµ Ð½Ð°Ð¹Ð´ÐµÐ½Ð¾',
			selectSearchPlaceholder: 'ÐŸÐ¾Ð¸ÑÐº...',
			selectVisibleOptions: 0,
			singleSelectzIndex: '2',
			selectSmartPositioning: true,
			onSelectOpened: function() {},
			onSelectClosed: function() {},
			onFormStyled: function() {}
		}, opt);

		return this.each(function() {
			var el = $(this);

			function attributes() {
				var id = '',
						title = '',
						classes = '',
						dataList = '';
				if (el.attr('id') !== undefined && el.attr('id') != '') id = ' id="' + el.attr('id') + opt.idSuffix + '"';
				if (el.attr('title') !== undefined && el.attr('title') != '') title = ' title="' + el.attr('title') + '"';
				if (el.attr('class') !== undefined && el.attr('class') != '') classes = ' ' + el.attr('class');
				var data = el.data();
				for (var i in data) {
					if (data[i] != '') dataList += ' data-' + i + '="' + data[i] + '"';
				}
				id += dataList;
				this.id = id;
				this.title = title;
				this.classes = classes;
			}

			// checkbox
			if (el.is(':checkbox')) {
				el.each(function() {
					if (el.parent('div.jq-checkbox').length < 1) {

						function checkbox() {

							var att = new attributes();
							var checkbox = $('<div' + att.id + ' class="jq-checkbox' + att.classes + '"' + att.title + '><div class="jq-checkbox__div"></div></div>');

							// Ð¿Ñ€ÑÑ‡ÐµÐ¼ Ð¾Ñ€Ð¸Ð³Ð¸Ð½Ð°Ð»ÑŒÐ½Ñ‹Ð¹ Ñ‡ÐµÐºÐ±Ð¾ÐºÑ
							el.css({
								position: 'absolute',
								zIndex: '-1',
								opacity: 0,
								margin: 0,
								padding: 0
							}).after(checkbox).prependTo(checkbox);

							checkbox.attr('unselectable', 'on').css({
								'-webkit-user-select': 'none',
								'-moz-user-select': 'none',
								'-ms-user-select': 'none',
								'-o-user-select': 'none',
								'user-select': 'none',
								display: 'inline-block',
								position: 'relative',
								overflow: 'hidden'
							});

							if (el.is(':checked')) checkbox.addClass('checked');
							if (el.is(':disabled')) checkbox.addClass('disabled');

							// ÐºÐ»Ð¸Ðº Ð½Ð° Ð¿ÑÐµÐ²Ð´Ð¾Ñ‡ÐµÐºÐ±Ð¾ÐºÑ
							checkbox.click(function() {
								if (!checkbox.is('.disabled')) {
									if (el.is(':checked')) {
										el.prop('checked', false);
										checkbox.removeClass('checked');
									} else {
										el.prop('checked', true);
										checkbox.addClass('checked');
									}
									el.change();
									return false;
								} else {
									return false;
								}
							});
							// ÐºÐ»Ð¸Ðº Ð½Ð° label
							el.closest('label').add('label[for="' + el.attr('id') + '"]').click(function(e) {
								checkbox.click();
								e.preventDefault();
							});
							// Ð¿ÐµÑ€ÐµÐºÐ»ÑŽÑ‡ÐµÐ½Ð¸Ðµ Ð¿Ð¾ Space Ð¸Ð»Ð¸ Enter
							el.change(function() {
								if (el.is(':checked')) checkbox.addClass('checked');
								else checkbox.removeClass('checked');
							})
							// Ñ‡Ñ‚Ð¾Ð±Ñ‹ Ð¿ÐµÑ€ÐµÐºÐ»ÑŽÑ‡Ð°Ð»ÑÑ Ñ‡ÐµÐºÐ±Ð¾ÐºÑ, ÐºÐ¾Ñ‚Ð¾Ñ€Ñ‹Ð¹ Ð½Ð°Ñ…Ð¾Ð´Ð¸Ñ‚ÑÑ Ð² Ñ‚ÐµÐ³Ðµ label
							.keydown(function(e) {
								if (e.which == 32) checkbox.click();
							})
							.focus(function() {
								if (!checkbox.is('.disabled')) checkbox.addClass('focused');
							})
							.blur(function() {
								checkbox.removeClass('focused');
							})

						} // end checkbox()

						checkbox();

						// Ð¾Ð±Ð½Ð¾Ð²Ð»ÐµÐ½Ð¸Ðµ Ð¿Ñ€Ð¸ Ð´Ð¸Ð½Ð°Ð¼Ð¸Ñ‡ÐµÑÐºÐ¾Ð¼ Ð¸Ð·Ð¼ÐµÐ½ÐµÐ½Ð¸Ð¸
						el.on('refresh', function() {
							el.parent().before(el).remove();
							checkbox();
						});

					}
				});
			// end checkbox

			// radio
			} else if (el.is(':radio')) {
				el.each(function() {
					if (el.parent('div.jq-radio').length < 1) {

						function radio() {

							var att = new attributes();
							var radio = $('<div' + att.id + ' class="jq-radio' + att.classes + '"' + att.title + '><div class="jq-radio__div"></div></div>');

							// Ð¿Ñ€ÑÑ‡ÐµÐ¼ Ð¾Ñ€Ð¸Ð³Ð¸Ð½Ð°Ð»ÑŒÐ½ÑƒÑŽ Ñ€Ð°Ð´Ð¸Ð¾ÐºÐ½Ð¾Ð¿ÐºÑƒ
							el.css({
								position: 'absolute',
								zIndex: '-1',
								opacity: 0,
								margin: 0,
								padding: 0
							}).after(radio).prependTo(radio);

							radio.attr('unselectable', 'on').css({
								'-webkit-user-select': 'none',
								'-moz-user-select': 'none',
								'-ms-user-select': 'none',
								'-o-user-select': 'none',
								'user-select': 'none',
								display: 'inline-block',
								position: 'relative'
							});

							if (el.is(':checked')) radio.addClass('checked');
							if (el.is(':disabled')) radio.addClass('disabled');

							// ÐºÐ»Ð¸Ðº Ð½Ð° Ð¿ÑÐµÐ²Ð´Ð¾Ñ€Ð°Ð´Ð¸Ð¾ÐºÐ½Ð¾Ð¿ÐºÐµ
							radio.click(function() {
								if (!radio.is('.disabled')) {
									radio.closest(opt.wrapper).find('input[name="' + el.attr('name') + '"]').prop('checked', false).parent().removeClass('checked');
									el.prop('checked', true).parent().addClass('checked');
									el.change();
									return false;
								} else {
									return false;
								}
							});
							// ÐºÐ»Ð¸Ðº Ð½Ð° label
							el.closest('label').add('label[for="' + el.attr('id') + '"]').click(function(e) {
								radio.click();
								e.preventDefault();
							});
							// Ð¿ÐµÑ€ÐµÐºÐ»ÑŽÑ‡ÐµÐ½Ð¸Ðµ ÑÑ‚Ñ€ÐµÐ»ÐºÐ°Ð¼Ð¸
							el.change(function() {
								el.parent().addClass('checked');
							})
							.focus(function() {
								if (!radio.is('.disabled')) radio.addClass('focused');
							})
							.blur(function() {
								radio.removeClass('focused');
							})

						} // end radio()

						radio();

						// Ð¾Ð±Ð½Ð¾Ð²Ð»ÐµÐ½Ð¸Ðµ Ð¿Ñ€Ð¸ Ð´Ð¸Ð½Ð°Ð¼Ð¸Ñ‡ÐµÑÐºÐ¾Ð¼ Ð¸Ð·Ð¼ÐµÐ½ÐµÐ½Ð¸Ð¸
						el.on('refresh', function() {
							el.parent().before(el).remove();
							radio();
						});

					}
				});
			// end radio

			// file
			} else if (el.is(':file')) {
				// Ð¿Ñ€ÑÑ‡ÐµÐ¼ Ð¾Ñ€Ð¸Ð³Ð¸Ð½Ð°Ð»ÑŒÐ½Ð¾Ðµ Ð¿Ð¾Ð»Ðµ
				el.css({
					position: 'absolute',
					top: 0,
					right: 0,
					width: '100%',
					height: '100%',
					opacity: 0,
					margin: 0,
					padding: 0
				}).each(function() {
					if (el.parent('div.jq-file').length < 1) {

						function file() {

							var att = new attributes();
							var file = $('<div' + att.id + ' class="jq-file' + att.classes + '"' + att.title + ' style="display: inline-block; position: relative; overflow: hidden"></div>');
							var name = $('<div class="jq-file__name">' + opt.filePlaceholder + '</div>').appendTo(file);
							var browse = $('<div class="jq-file__browse">' + opt.fileBrowse + '</div>').appendTo(file);
							el.after(file);
							file.append(el);
							if (el.is(':disabled')) file.addClass('disabled');
							el.change(function() {
								name.text(el.val().replace(/.+[\\\/]/, ''));
								if (el.val() == '') {
									name.text(opt.filePlaceholder);
									file.removeClass('changed');
								} else {
									file.addClass('changed');
								}
							})
							.focus(function() {
								file.addClass('focused');
							})
							.blur(function() {
								file.removeClass('focused');
							})
							.click(function() {
								file.removeClass('focused');
							})

						} // end file()

						file();

						// Ð¾Ð±Ð½Ð¾Ð²Ð»ÐµÐ½Ð¸Ðµ Ð¿Ñ€Ð¸ Ð´Ð¸Ð½Ð°Ð¼Ð¸Ñ‡ÐµÑÐºÐ¾Ð¼ Ð¸Ð·Ð¼ÐµÐ½ÐµÐ½Ð¸Ð¸
						el.on('refresh', function() {
							el.parent().before(el).remove();
							file();
						})

					}
				});
			// end file

			// select
			} else if (el.is('select')) {
				el.each(function() {
					if (el.parent('div.jqselect').length < 1) {

						function selectbox() {

							// Ð·Ð°Ð¿Ñ€ÐµÑ‰Ð°ÐµÐ¼ Ð¿Ñ€Ð¾ÐºÑ€ÑƒÑ‚ÐºÑƒ ÑÑ‚Ñ€Ð°Ð½Ð¸Ñ†Ñ‹ Ð¿Ñ€Ð¸ Ð¿Ñ€Ð¾ÐºÑ€ÑƒÑ‚ÐºÐµ ÑÐµÐ»ÐµÐºÑ‚Ð°
							function preventScrolling(selector) {
								selector.off('mousewheel DOMMouseScroll').on('mousewheel DOMMouseScroll', function(e) {
									var scrollTo = null;
									if (e.type == 'mousewheel') { scrollTo = (e.originalEvent.wheelDelta * -1); }
									else if (e.type == 'DOMMouseScroll') { scrollTo = 40 * e.originalEvent.detail; }
									if (scrollTo) {
										e.stopPropagation();
										e.preventDefault();
										$(this).scrollTop(scrollTo + $(this).scrollTop());
									}
								});
							}

							var option = $('option', el);
							var list = '';
							// Ñ„Ð¾Ñ€Ð¼Ð¸Ñ€ÑƒÐµÐ¼ ÑÐ¿Ð¸ÑÐ¾Ðº ÑÐµÐ»ÐµÐºÑ‚Ð°
							function makeList() {
								for (i = 0, len = option.length; i < len; i++) {
									var li = '',
											liClass = '',
											dataList = '',
											optionClass = '',
											optgroupClass = '',
											dataJqfsClass = '';
									var disabled = 'disabled';
									var selDis = 'selected sel disabled';
									if (option.eq(i).prop('selected')) liClass = 'selected sel';
									if (option.eq(i).is(':disabled')) liClass = disabled;
									if (option.eq(i).is(':selected:disabled')) liClass = selDis;
									if (option.eq(i).attr('class') !== undefined) {
										optionClass = ' ' + option.eq(i).attr('class');
										dataJqfsClass = ' data-jqfs-class="' + option.eq(i).attr('class') + '"';
									}

									var data = option.eq(i).data();
									for (var k in data) {
										if (data[k] != '') dataList += ' data-' + k + '="' + data[k] + '"';
									}

									li = '<li' + dataJqfsClass + dataList + ' class="' + liClass + optionClass + '">'+ option.eq(i).text() +'</li>';

									// ÐµÑÐ»Ð¸ ÐµÑÑ‚ÑŒ optgroup
									if (option.eq(i).parent().is('optgroup')) {
										if (option.eq(i).parent().attr('class') !== undefined) optgroupClass = ' ' + option.eq(i).parent().attr('class');
										li = '<li' + dataJqfsClass + ' class="' + liClass + optionClass + ' option' + optgroupClass + '">'+ option.eq(i).text() +'</li>';
										if (option.eq(i).is(':first-child')) {
											li = '<li class="optgroup' + optgroupClass + '">' + option.eq(i).parent().attr('label') + '</li>' + li;
										}
									}

									list += li;
								}
							} // end makeList()

							// Ð¾Ð´Ð¸Ð½Ð¾Ñ‡Ð½Ñ‹Ð¹ ÑÐµÐ»ÐµÐºÑ‚
							function doSelect() {
								var att = new attributes();
								var selectbox =
									$('<div' + att.id + ' class="jq-selectbox jqselect' + att.classes + '" style="display: inline-block; position: relative; z-index:' + opt.singleSelectzIndex + '">' +
											'<div class="jq-selectbox__select"' + att.title + ' style="position: relative">' +
												'<div class="jq-selectbox__select-text"></div>' +
												'<div class="jq-selectbox__trigger"><div class="jq-selectbox__trigger-arrow"></div></div>' +
											'</div>' +
										'</div>');

								el.css({margin: 0, padding: 0}).after(selectbox).prependTo(selectbox);

								var divSelect = $('div.jq-selectbox__select', selectbox);
								var divText = $('div.jq-selectbox__select-text', selectbox);
								var optionSelected = option.filter(':selected');

								// Ð±ÐµÑ€ÐµÐ¼ Ð¾Ð¿Ñ†Ð¸ÑŽ Ð¿Ð¾ ÑƒÐ¼Ð¾Ð»Ñ‡Ð°Ð½Ð¸ÑŽ
								if (optionSelected.length) {
									divText.html(optionSelected.text());
								} else {
									divText.html(option.first().text());
								}

								makeList();
								var searchHTML = '';
								if (opt.selectSearch) searchHTML =
									'<div class="jq-selectbox__search"><input type="search" autocomplete="off" placeholder="' + opt.selectSearchPlaceholder + '"></div>' +
									'<div class="jq-selectbox__not-found">' + opt.selectSearchNotFound + '</div>';
								var dropdown =
									$('<div class="jq-selectbox__dropdown" style="position: absolute">' +
											searchHTML +
											'<ul style="position: relative; list-style: none; overflow: auto; overflow-x: hidden">' + list + '</ul>' +
										'</div>');
								selectbox.append(dropdown);
								var ul = $('ul', dropdown);
								var li = $('li', dropdown);
								var search = $('input', dropdown);
								var notFound = $('div.jq-selectbox__not-found', dropdown).hide();
								if (li.length < opt.selectSearchLimit) search.parent().hide();

								// Ð¾Ð¿Ñ€ÐµÐ´ÐµÐ»ÑÐµÐ¼ ÑÐ°Ð¼Ñ‹Ð¹ ÑˆÐ¸Ñ€Ð¾ÐºÐ¸Ð¹ Ð¿ÑƒÐ½ÐºÑ‚ ÑÐµÐ»ÐµÐºÑ‚Ð°
								var liWidth1 = 0,
										liWidth2 = 0;
								li.each(function() {
									var l = $(this);
									l.css({'display': 'inline-block', 'white-space': 'nowrap'});
									if (l.innerWidth() > liWidth1) {
										liWidth1 = l.innerWidth();
										liWidth2 = l.width();
									}
									l.css({'display': 'block'});
								});

								// Ð¿Ð¾Ð´ÑÑ‚Ñ€Ð°Ð¸Ð²Ð°ÐµÐ¼ ÑˆÐ¸Ñ€Ð¸Ð½Ñƒ Ð¿ÑÐµÐ²Ð´Ð¾ÑÐµÐ»ÐµÐºÑ‚Ð° Ð¸ Ð²Ñ‹Ð¿Ð°Ð´Ð°ÑŽÑ‰ÐµÐ³Ð¾ ÑÐ¿Ð¸ÑÐºÐ°
								// Ð² Ð·Ð°Ð²Ð¸ÑÐ¸Ð¼Ð¾ÑÑ‚Ð¸ Ð¾Ñ‚ ÑÐ°Ð¼Ð¾Ð³Ð¾ ÑˆÐ¸Ñ€Ð¾ÐºÐ¾Ð³Ð¾ Ð¿ÑƒÐ½ÐºÑ‚Ð°
								var selClone = selectbox.clone().appendTo('body').width('auto');
								var selCloneWidth = selClone.width();
								selClone.remove();
								if (selCloneWidth == selectbox.width()) {
									divText.width(liWidth2);
									liWidth1 += selectbox.find('div.jq-selectbox__trigger').width();
								}
								if ( liWidth1 > selectbox.width() ) {
									dropdown.width(liWidth1);
								}

								// Ð¿Ñ€ÑÑ‡ÐµÐ¼ Ð¾Ñ€Ð¸Ð³Ð¸Ð½Ð°Ð»ÑŒÐ½Ñ‹Ð¹ ÑÐµÐ»ÐµÐºÑ‚
								el.css({
									position: 'absolute',
									left: 0,
									top: 0,
									width: '100%',
									height: '100%',
									opacity: 0
								});

								var selectHeight = selectbox.outerHeight();
								var searchHeight = search.outerHeight();
								var isMaxHeight = ul.css('max-height');
								var liSelected = li.filter('.selected');
								if (liSelected.length < 1) li.first().addClass('selected sel');
								if (li.data('li-height') === undefined) li.data('li-height', li.outerHeight());
								var position = dropdown.css('top');
								if (dropdown.css('left') == 'auto') dropdown.css({left: 0});
								if (dropdown.css('top') == 'auto') dropdown.css({top: selectHeight});
								dropdown.hide();

								// ÐµÑÐ»Ð¸ Ð²Ñ‹Ð±Ñ€Ð°Ð½ Ð½Ðµ Ð´ÐµÑ„Ð¾Ð»Ñ‚Ð½Ñ‹Ð¹ Ð¿ÑƒÐ½ÐºÑ‚
								if (liSelected.length) {
									// Ð´Ð¾Ð±Ð°Ð²Ð»ÑÐµÐ¼ ÐºÐ»Ð°ÑÑ, Ð¿Ð¾ÐºÐ°Ð·Ñ‹Ð²Ð°ÑŽÑ‰Ð¸Ð¹ Ð¸Ð·Ð¼ÐµÐ½ÐµÐ½Ð¸Ðµ ÑÐµÐ»ÐµÐºÑ‚Ð°
									if (option.first().text() != optionSelected.text()) {
										selectbox.addClass('changed');
									}
									// Ð¿ÐµÑ€ÐµÐ´Ð°ÐµÐ¼ ÑÐµÐ»ÐµÐºÑ‚Ñƒ ÐºÐ»Ð°ÑÑ Ð²Ñ‹Ð±Ñ€Ð°Ð½Ð½Ð¾Ð³Ð¾ Ð¿ÑƒÐ½ÐºÑ‚Ð°
									selectbox.data('jqfs-class', liSelected.data('jqfs-class'));
									selectbox.addClass(liSelected.data('jqfs-class'));
								}

								// ÐµÑÐ»Ð¸ ÑÐµÐ»ÐµÐºÑ‚ Ð½ÐµÐ°ÐºÑ‚Ð¸Ð²Ð½Ñ‹Ð¹
								if (el.is(':disabled')) {
									selectbox.addClass('disabled');
									return false;
								}

								// Ð¿Ñ€Ð¸ ÐºÐ»Ð¸ÐºÐµ Ð½Ð° Ð¿ÑÐµÐ²Ð´Ð¾ÑÐµÐ»ÐµÐºÑ‚Ðµ
								divSelect.click(function() {
									el.focus();

									// ÐºÐ¾Ð»Ð±ÐµÐº Ð¿Ñ€Ð¸ Ð·Ð°ÐºÑ€Ñ‹Ñ‚Ð¸Ð¸ ÑÐµÐ»ÐµÐºÑ‚Ð°
									if ($('div.jq-selectbox').filter('.opened').length) {
										opt.onSelectClosed.call($('div.jq-selectbox').filter('.opened'));
									}

									// ÐµÑÐ»Ð¸ iOS, Ñ‚Ð¾ Ð½Ðµ Ð¿Ð¾ÐºÐ°Ð·Ñ‹Ð²Ð°ÐµÐ¼ Ð²Ñ‹Ð¿Ð°Ð´Ð°ÑŽÑ‰Ð¸Ð¹ ÑÐ¿Ð¸ÑÐ¾Ðº
									var iOS = navigator.userAgent.match(/(iPad|iPhone|iPod)/g) ? true : false;
									if (iOS) return;

									// ÑƒÐ¼Ð½Ð¾Ðµ Ð¿Ð¾Ð·Ð¸Ñ†Ð¸Ð¾Ð½Ð¸Ñ€Ð¾Ð²Ð°Ð½Ð¸Ðµ
									if (opt.selectSmartPositioning) {
										var win = $(window);
										var topOffset = selectbox.offset().top;
										var bottomOffset = win.height() - selectHeight - (topOffset - win.scrollTop());
										var visible = opt.selectVisibleOptions;
										var liHeight = li.data('li-height');
										var	minHeight = liHeight * 5;
										var	newHeight = liHeight * visible;
										if (visible > 0 && visible < 6) minHeight = newHeight;
										if (visible == 0) newHeight = 'auto';

										// Ñ€Ð°ÑÐºÑ€Ñ‹Ñ‚Ð¸Ðµ Ð²Ð½Ð¸Ð·
										if (bottomOffset > (minHeight + searchHeight + 20))	{
											dropdown.height('auto').css({bottom: 'auto', top: position});
											function maxHeightBottom() {
												ul.css('max-height', Math.floor((bottomOffset - 20 - searchHeight) / liHeight) * liHeight);
											}
											maxHeightBottom();
											ul.css('max-height', newHeight);
											if (isMaxHeight != 'none') {
												ul.css('max-height', isMaxHeight);
											}
											if (bottomOffset < (dropdown.outerHeight() + 20)) {
												maxHeightBottom();
											}

										// Ñ€Ð°ÑÐºÑ€Ñ‹Ñ‚Ð¸Ðµ Ð²Ð²ÐµÑ€Ñ…
										} else {
											dropdown.height('auto').css({top: 'auto', bottom: position});
											function maxHeightTop() {
												ul.css('max-height', Math.floor((topOffset - win.scrollTop() - 20 - searchHeight) / liHeight) * liHeight);
											}
											maxHeightTop();
											ul.css('max-height', newHeight);
											if (isMaxHeight != 'none') {
												ul.css('max-height', isMaxHeight);
											}
											if ((topOffset - win.scrollTop() - 20) < (dropdown.outerHeight() + 20)) {
												maxHeightTop();
											}
										}
									}

									$('div.jqselect').css({zIndex: (opt.singleSelectzIndex - 1)}).removeClass('opened');
									selectbox.css({zIndex: opt.singleSelectzIndex});
									if (dropdown.is(':hidden')) {
										$('div.jq-selectbox__dropdown:visible').hide();
										dropdown.show();
										selectbox.addClass('opened focused');
										// ÐºÐ¾Ð»Ð±ÐµÐº Ð¿Ñ€Ð¸ Ð¾Ñ‚ÐºÑ€Ñ‹Ñ‚Ð¸Ð¸ ÑÐµÐ»ÐµÐºÑ‚Ð°
										opt.onSelectOpened.call(selectbox);
									} else {
										dropdown.hide();
										selectbox.removeClass('opened');
										// ÐºÐ¾Ð»Ð±ÐµÐº Ð¿Ñ€Ð¸ Ð·Ð°ÐºÑ€Ñ‹Ñ‚Ð¸Ð¸ ÑÐµÐ»ÐµÐºÑ‚Ð°
										if ($('div.jq-selectbox').filter('.opened').length) {
											opt.onSelectClosed.call(selectbox);
										}
									}

									// Ð¿Ñ€Ð¾ÐºÑ€ÑƒÑ‡Ð¸Ð²Ð°ÐµÐ¼ Ð´Ð¾ Ð²Ñ‹Ð±Ñ€Ð°Ð½Ð½Ð¾Ð³Ð¾ Ð¿ÑƒÐ½ÐºÑ‚Ð° Ð¿Ñ€Ð¸ Ð¾Ñ‚ÐºÑ€Ñ‹Ñ‚Ð¸Ð¸ ÑÐ¿Ð¸ÑÐºÐ°
									if (li.filter('.selected').length) {
										// ÐµÑÐ»Ð¸ Ð½ÐµÑ‡ÐµÑ‚Ð½Ð¾Ðµ ÐºÐ¾Ð»Ð¸Ñ‡ÐµÑÑ‚Ð²Ð¾ Ð²Ð¸Ð´Ð¸Ð¼Ñ‹Ñ… Ð¿ÑƒÐ½ÐºÑ‚Ð¾Ð²,
										// Ñ‚Ð¾ Ð²Ñ‹ÑÐ¾Ñ‚Ñƒ Ð¿ÑƒÐ½ÐºÑ‚Ð° Ð´ÐµÐ»Ð¸Ð¼ Ð¿Ð¾Ð¿Ð¾Ð»Ð°Ð¼ Ð´Ð»Ñ Ð¿Ð¾ÑÐ»ÐµÐ´ÑƒÑŽÑ‰ÐµÐ³Ð¾ Ñ€Ð°ÑÑ‡ÐµÑ‚Ð°
										if ( (ul.innerHeight() / liHeight) % 2 != 0 ) liHeight = liHeight / 2;
										ul.scrollTop(ul.scrollTop() + li.filter('.selected').position().top - ul.innerHeight() / 2 + liHeight);
									}

									// Ð¿Ð¾Ð¸ÑÐºÐ¾Ð²Ð¾Ðµ Ð¿Ð¾Ð»Ðµ
									if (search.length) {
										search.val('').keyup();
										notFound.hide();
										search.focus().keyup(function() {
											var query = $(this).val();
											li.each(function() {
												if (!$(this).html().match(new RegExp('.*?' + query + '.*?', 'i'))) {
													$(this).hide();
												} else {
													$(this).show();
												}
											});
											if (li.filter(':visible').length < 1) {
												notFound.show();
											} else {
												notFound.hide();
											}
										});
									}

									preventScrolling(ul);
									return false;
								});

								// Ð¿Ñ€Ð¸ Ð½Ð°Ð²ÐµÐ´ÐµÐ½Ð¸Ð¸ ÐºÑƒÑ€ÑÐ¾Ñ€Ð° Ð½Ð° Ð¿ÑƒÐ½ÐºÑ‚ ÑÐ¿Ð¸ÑÐºÐ°
								li.hover(function() {
									$(this).siblings().removeClass('selected');
								});
								var selectedText = li.filter('.selected').text();
								var selText = li.filter('.selected').text();

								// Ð¿Ñ€Ð¸ ÐºÐ»Ð¸ÐºÐµ Ð½Ð° Ð¿ÑƒÐ½ÐºÑ‚ ÑÐ¿Ð¸ÑÐºÐ°
								li.filter(':not(.disabled):not(.optgroup)').click(function() {
									var t = $(this);
									var liText = t.text();
									if (selectedText != liText) {
										var index = t.index();
										if (t.is('.option')) index -= t.prevAll('.optgroup').length;
										t.addClass('selected sel').siblings().removeClass('selected sel');
										option.prop('selected', false).eq(index).prop('selected', true);
										selectedText = liText;
										divText.html(liText);

										// Ð¿ÐµÑ€ÐµÐ´Ð°ÐµÐ¼ ÑÐµÐ»ÐµÐºÑ‚Ñƒ ÐºÐ»Ð°ÑÑ Ð²Ñ‹Ð±Ñ€Ð°Ð½Ð½Ð¾Ð³Ð¾ Ð¿ÑƒÐ½ÐºÑ‚Ð°
										if (selectbox.data('jqfs-class')) selectbox.removeClass(selectbox.data('jqfs-class'));
										selectbox.data('jqfs-class', t.data('jqfs-class'));
										selectbox.addClass(t.data('jqfs-class'));

										el.change();
									}
									if (search.length) {
										search.val('').keyup();
										notFound.hide();
									}
									dropdown.hide();
									selectbox.removeClass('opened');
									// ÐºÐ¾Ð»Ð±ÐµÐº Ð¿Ñ€Ð¸ Ð·Ð°ÐºÑ€Ñ‹Ñ‚Ð¸Ð¸ ÑÐµÐ»ÐµÐºÑ‚Ð°
									opt.onSelectClosed.call(selectbox);

								});
								dropdown.mouseout(function() {
									$('li.sel', dropdown).addClass('selected');
								});

								// Ð¸Ð·Ð¼ÐµÐ½ÐµÐ½Ð¸Ðµ ÑÐµÐ»ÐµÐºÑ‚Ð°
								el.change(function() {
									divText.html(option.filter(':selected').text());
									li.removeClass('selected sel').not('.optgroup').eq(el[0].selectedIndex).addClass('selected sel');
									// Ð´Ð¾Ð±Ð°Ð²Ð»ÑÐµÐ¼ ÐºÐ»Ð°ÑÑ, Ð¿Ð¾ÐºÐ°Ð·Ñ‹Ð²Ð°ÑŽÑ‰Ð¸Ð¹ Ð¸Ð·Ð¼ÐµÐ½ÐµÐ½Ð¸Ðµ ÑÐµÐ»ÐµÐºÑ‚Ð°
									if (option.first().text() != li.filter('.selected').text()) {
										selectbox.addClass('changed');
									} else {
										selectbox.removeClass('changed');
									}
								})
								.focus(function() {
									selectbox.addClass('focused');
									$('div.jqselect').removeClass('opened');
								})
								.blur(function() {
									selectbox.removeClass('focused');
								})
								// Ð¸Ð·Ð¼ÐµÐ½ÐµÐ½Ð¸Ðµ ÑÐµÐ»ÐµÐºÑ‚Ð° Ñ ÐºÐ»Ð°Ð²Ð¸Ð°Ñ‚ÑƒÑ€Ñ‹
								.on('keydown keyup', function(e) {
									divText.html(option.filter(':selected').text());
									li.removeClass('selected sel').not('.optgroup').eq(el[0].selectedIndex).addClass('selected sel');
									// Ð²Ð²ÐµÑ€Ñ…, Ð²Ð»ÐµÐ²Ð¾, PageUp
									if (e.which == 38 || e.which == 37 || e.which == 33) {
										dropdown.scrollTop(dropdown.scrollTop() + li.filter('.selected').position().top);
									}
									// Ð²Ð½Ð¸Ð·, Ð²Ð¿Ñ€Ð°Ð²Ð¾, PageDown
									if (e.which == 40 || e.which == 39 || e.which == 34) {
										dropdown.scrollTop(dropdown.scrollTop() + li.filter('.selected').position().top - dropdown.innerHeight() + liHeight);
									}
									// Ð¾Ñ‚ÐºÑ€Ñ‹Ð²Ð°ÐµÐ¼ Ð²Ñ‹Ð¿Ð°Ð´Ð°ÑŽÑ‰Ð¸Ð¹ ÑÐ¿Ð¸ÑÐ¾Ðº Ð¿Ñ€Ð¸ Ð½Ð°Ð¶Ð°Ñ‚Ð¸Ð¸ Space
									if (e.which == 32) {
										e.preventDefault();
										// Ð¼Ð¾Ð¶Ð½Ð¾ Ð±Ñ‹Ð»Ð¾ Ð±Ñ‹ Ð¾Ñ‚ÐºÑ€Ñ‹Ð²Ð°Ñ‚ÑŒ Ñ‡ÐµÑ€ÐµÐ· Ð·Ð°Ð¿ÑƒÑÐº divSelect.click(),
										// Ð½Ð¾ Ð¿Ð¾Ñ‡ÐµÐ¼Ñƒ-Ñ‚Ð¾ ÑÐ¿Ð¸ÑÐ¾Ðº Ð¿Ð¾ÑÐ»Ðµ Ð¾Ñ‚ÐºÑ€Ñ‹Ñ‚Ð¸Ñ ÑÑ€Ð°Ð·Ñƒ Ð·Ð°ÐºÑ€Ñ‹Ð²Ð°ÐµÑ‚ÑÑ
										// Ñ€ÐµÑˆÐµÐ½Ð¸Ðµ Ð¿Ð¾ÐºÐ° Ð½Ðµ Ð½Ð°Ð¹Ð´ÐµÐ½Ð¾
										// divSelect.click();
									}
									// Ð·Ð°ÐºÑ€Ñ‹Ð²Ð°ÐµÐ¼ Ð²Ñ‹Ð¿Ð°Ð´Ð°ÑŽÑ‰Ð¸Ð¹ ÑÐ¿Ð¸ÑÐ¾Ðº Ð¿Ñ€Ð¸ Ð½Ð°Ð¶Ð°Ñ‚Ð¸Ð¸ Enter
									if (e.which == 13) {
										e.preventDefault();
										dropdown.hide();
									}
								});

								// Ð¿Ñ€ÑÑ‡ÐµÐ¼ Ð²Ñ‹Ð¿Ð°Ð´Ð°ÑŽÑ‰Ð¸Ð¹ ÑÐ¿Ð¸ÑÐ¾Ðº Ð¿Ñ€Ð¸ ÐºÐ»Ð¸ÐºÐµ Ð·Ð° Ð¿Ñ€ÐµÐ´ÐµÐ»Ð°Ð¼Ð¸ ÑÐµÐ»ÐµÐºÑ‚Ð°
								$(document).on('click', function(e) {
									// e.target.nodeName != 'OPTION' - Ð´Ð¾Ð±Ð°Ð²Ð»ÐµÐ½Ð¾ Ð´Ð»Ñ Ð¾Ð±Ñ…Ð¾Ð´Ð° Ð±Ð°Ð³Ð° Ð² Opera Ð½Ð° Ð´Ð²Ð¸Ð¶ÐºÐµ Presto
									// (Ð¿Ñ€Ð¸ Ð¸Ð·Ð¼ÐµÐ½ÐµÐ½Ð¸Ð¸ ÑÐµÐ»ÐµÐºÑ‚Ð° Ñ ÐºÐ»Ð°Ð²Ð¸Ð°Ñ‚ÑƒÑ€Ñ‹ ÑÑ€Ð°Ð±Ð°Ñ‚Ñ‹Ð²Ð°ÐµÑ‚ ÑÐ¾Ð±Ñ‹Ñ‚Ð¸Ðµ onclick)
									if (!$(e.target).parents().hasClass('jq-selectbox') && e.target.nodeName != 'OPTION') {

										// ÐºÐ¾Ð»Ð±ÐµÐº Ð¿Ñ€Ð¸ Ð·Ð°ÐºÑ€Ñ‹Ñ‚Ð¸Ð¸ ÑÐµÐ»ÐµÐºÑ‚Ð°
										if ($('div.jq-selectbox').filter('.opened').length) {
											opt.onSelectClosed.call($('div.jq-selectbox').filter('.opened'));
										}

										if (search.length) search.val('').keyup();
										dropdown.hide().find('li.sel').addClass('selected');
										selectbox.removeClass('focused opened');

									}
								});

							} // end doSelect()

							// Ð¼ÑƒÐ»ÑŒÑ‚Ð¸ÑÐµÐ»ÐµÐºÑ‚
							function doMultipleSelect() {
								var att = new attributes();
								var selectbox = $('<div' + att.id + ' class="jq-select-multiple jqselect' + att.classes + '"' + att.title + ' style="display: inline-block; position: relative"></div>');

								el.css({margin: 0, padding: 0}).after(selectbox);

								makeList();
								selectbox.append('<ul>' + list + '</ul>');
								var ul = $('ul', selectbox).css({
									'position': 'relative',
									'overflow-x': 'hidden',
									'-webkit-overflow-scrolling': 'touch'
								});
								var li = $('li', selectbox).attr('unselectable', 'on').css({'-webkit-user-select': 'none', '-moz-user-select': 'none', '-ms-user-select': 'none', '-o-user-select': 'none', 'user-select': 'none', 'white-space': 'nowrap'});
								var size = el.attr('size');
								var ulHeight = ul.outerHeight();
								var liHeight = li.outerHeight();
								if (size !== undefined && size > 0) {
									ul.css({'height': liHeight * size});
								} else {
									ul.css({'height': liHeight * 4});
								}
								if (ulHeight > selectbox.height()) {
									ul.css('overflowY', 'scroll');
									preventScrolling(ul);
									// Ð¿Ñ€Ð¾ÐºÑ€ÑƒÑ‡Ð¸Ð²Ð°ÐµÐ¼ Ð´Ð¾ Ð²Ñ‹Ð±Ñ€Ð°Ð½Ð½Ð¾Ð³Ð¾ Ð¿ÑƒÐ½ÐºÑ‚Ð°
									if (li.filter('.selected').length) {
										ul.scrollTop(ul.scrollTop() + li.filter('.selected').position().top);
									}
								}

								// Ð¿Ñ€ÑÑ‡ÐµÐ¼ Ð¾Ñ€Ð¸Ð³Ð¸Ð½Ð°Ð»ÑŒÐ½Ñ‹Ð¹ ÑÐµÐ»ÐµÐºÑ‚
								el.prependTo(selectbox).css({
									position: 'absolute',
									left: 0,
									top: 0,
									width: '100%',
									height: '100%',
									opacity: 0
								});

								// ÐµÑÐ»Ð¸ ÑÐµÐ»ÐµÐºÑ‚ Ð½ÐµÐ°ÐºÑ‚Ð¸Ð²Ð½Ñ‹Ð¹
								if (el.is(':disabled')) {
									selectbox.addClass('disabled');
									option.each(function() {
										if ($(this).is(':selected')) li.eq($(this).index()).addClass('selected');
									});

								// ÐµÑÐ»Ð¸ ÑÐµÐ»ÐµÐºÑ‚ Ð°ÐºÑ‚Ð¸Ð²Ð½Ñ‹Ð¹
								} else {

									// Ð¿Ñ€Ð¸ ÐºÐ»Ð¸ÐºÐµ Ð½Ð° Ð¿ÑƒÐ½ÐºÑ‚ ÑÐ¿Ð¸ÑÐºÐ°
									li.filter(':not(.disabled):not(.optgroup)').click(function(e) {
										el.focus();
										selectbox.removeClass('focused');
										var clkd = $(this);
										if(!e.ctrlKey && !e.metaKey) clkd.addClass('selected');
										if(!e.shiftKey) clkd.addClass('first');
										if(!e.ctrlKey && !e.metaKey && !e.shiftKey) clkd.siblings().removeClass('selected first');

										// Ð²Ñ‹Ð´ÐµÐ»ÐµÐ½Ð¸Ðµ Ð¿ÑƒÐ½ÐºÑ‚Ð¾Ð² Ð¿Ñ€Ð¸ Ð·Ð°Ð¶Ð°Ñ‚Ð¾Ð¼ Ctrl
										if(e.ctrlKey || e.metaKey) {
											if (clkd.is('.selected')) clkd.removeClass('selected first');
												else clkd.addClass('selected first');
											clkd.siblings().removeClass('first');
										}

										// Ð²Ñ‹Ð´ÐµÐ»ÐµÐ½Ð¸Ðµ Ð¿ÑƒÐ½ÐºÑ‚Ð¾Ð² Ð¿Ñ€Ð¸ Ð·Ð°Ð¶Ð°Ñ‚Ð¾Ð¼ Shift
										if(e.shiftKey) {
											var prev = false,
													next = false;
											clkd.siblings().removeClass('selected').siblings('.first').addClass('selected');
											clkd.prevAll().each(function() {
												if ($(this).is('.first')) prev = true;
											});
											clkd.nextAll().each(function() {
												if ($(this).is('.first')) next = true;
											});
											if (prev) {
												clkd.prevAll().each(function() {
													if ($(this).is('.selected')) return false;
														else $(this).not('.disabled, .optgroup').addClass('selected');
												});
											}
											if (next) {
												clkd.nextAll().each(function() {
													if ($(this).is('.selected')) return false;
														else $(this).not('.disabled, .optgroup').addClass('selected');
												});
											}
											if (li.filter('.selected').length == 1) clkd.addClass('first');
										}

										// Ð¾Ñ‚Ð¼ÐµÑ‡Ð°ÐµÐ¼ Ð²Ñ‹Ð±Ñ€Ð°Ð½Ð½Ñ‹Ðµ Ð¼Ñ‹ÑˆÑŒÑŽ
										option.prop('selected', false);
										li.filter('.selected').each(function() {
											var t = $(this);
											var index = t.index();
											if (t.is('.option')) index -= t.prevAll('.optgroup').length;
											option.eq(index).prop('selected', true);
										});
										el.change();

									});

									// Ð¾Ñ‚Ð¼ÐµÑ‡Ð°ÐµÐ¼ Ð²Ñ‹Ð±Ñ€Ð°Ð½Ð½Ñ‹Ðµ Ñ ÐºÐ»Ð°Ð²Ð¸Ð°Ñ‚ÑƒÑ€Ñ‹
									option.each(function(i) {
										$(this).data('optionIndex', i);
									});
									el.change(function() {
										li.removeClass('selected');
										var arrIndexes = [];
										option.filter(':selected').each(function() {
											arrIndexes.push($(this).data('optionIndex'));
										});
										li.not('.optgroup').filter(function(i) {
											return $.inArray(i, arrIndexes) > -1;
										}).addClass('selected');
									})
									.focus(function() {
										selectbox.addClass('focused');
									})
									.blur(function() {
										selectbox.removeClass('focused');
									});

									// Ð¿Ñ€Ð¾ÐºÑ€ÑƒÑ‡Ð¸Ð²Ð°ÐµÐ¼ Ñ ÐºÐ»Ð°Ð²Ð¸Ð°Ñ‚ÑƒÑ€Ñ‹
									if (ulHeight > selectbox.height()) {
										el.keydown(function(e) {
											// Ð²Ð²ÐµÑ€Ñ…, Ð²Ð»ÐµÐ²Ð¾, PageUp
											if (e.which == 38 || e.which == 37 || e.which == 33) {
												ul.scrollTop(ul.scrollTop() + li.filter('.selected').position().top - liHeight);
											}
											// Ð²Ð½Ð¸Ð·, Ð²Ð¿Ñ€Ð°Ð²Ð¾, PageDown
											if (e.which == 40 || e.which == 39 || e.which == 34) {
												ul.scrollTop(ul.scrollTop() + li.filter('.selected:last').position().top - ul.innerHeight() + liHeight * 2);
											}
										});
									}

								}
							} // end doMultipleSelect()
							if (el.is('[multiple]')) doMultipleSelect(); else doSelect();
						} // end selectbox()

						selectbox();

						// Ð¾Ð±Ð½Ð¾Ð²Ð»ÐµÐ½Ð¸Ðµ Ð¿Ñ€Ð¸ Ð´Ð¸Ð½Ð°Ð¼Ð¸Ñ‡ÐµÑÐºÐ¾Ð¼ Ð¸Ð·Ð¼ÐµÐ½ÐµÐ½Ð¸Ð¸
						el.on('refresh', function() {
							el.parent().before(el).remove();
							selectbox();
						});

					}
				});
			// end select

			// reset
			} else if (el.is(':reset')) {
				el.click(function() {
					setTimeout(function() {
						el.closest(opt.wrapper).find('input, select').trigger('refresh');
					}, 1)
				});
			}
			// end reset

		})

		// ÐºÐ¾Ð»Ð±ÐµÐº Ð¿Ð¾ÑÐ»Ðµ Ð²Ñ‹Ð¿Ð¾Ð»Ð½ÐµÐ½Ð¸Ñ Ð¿Ð»Ð°Ð³Ð¸Ð½Ð°
		.promise()
		.done(function() {
			opt.onFormStyled.call();
		});

	}
})(jQuery);